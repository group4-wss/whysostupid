package fr.group4.whysostupid;

import org.hamcrest.core.IsEqual;
import org.junit.Before;
import org.junit.Test;

import fr.group4.whysostupid.Model.Category;
import static org.junit.Assert.*;

public class CategoryUnitTest {
    Category cat1;
    Object subscribe;

    @Before
    public void setUpCategory() {cat1 = new Category("Name", "Image", subscribe);}

    @Test
    public void getName() {assertEquals(cat1.getName(), "Name");}

    @Test
    public void getImage() {assertEquals(cat1.getImage(), "Image");}

    @Test
    public void setName() {cat1.setName("Name2"); assertNotEquals(cat1.getName(), "Name");}

    @Test
    public void setImage() {cat1.setImage("Image2"); assertNotEquals(cat1.getImage(), "Image");}
}
