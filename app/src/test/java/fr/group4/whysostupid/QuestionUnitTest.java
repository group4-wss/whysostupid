package fr.group4.whysostupid;

import org.hamcrest.core.IsEqual;
import org.junit.Before;
import org.junit.Test;

import fr.group4.whysostupid.Model.Question;

import static org.junit.Assert.*;

public class QuestionUnitTest {
    Question quest1;

    @Before
    public void setUpQuestion() {quest1 = new Question("Ceci est une question ?", "Réponse A", "Réponse B", "Réponse C", "Réponse D", "Réponse A", "1", "Oui");}

    @Test
    public void getQuestion() {assertEquals(quest1.getCorrectAnswer(), "Réponse A"); assertEquals(quest1.getCategoryId(), "1"); assertEquals(quest1.getIsImageQuestion(), "Oui");}

    @Test
    public void setQuestion() {quest1.setQuestion("Est-ce que ceci est une question ?"); assertNotEquals(quest1.getQuestion(), "Ceci est une question ?"); quest1.setCorrectAnswer("Réponse B"); assertNotEquals(quest1.getCorrectAnswer(), "Réponse A");}

    @Test
    public void setCatAndImage() {quest1.setCategoryId("2"); assertNotEquals(quest1.getCategoryId(), "1"); quest1.setIsImageQuestion("Non"); assertNotEquals(quest1.getIsImageQuestion(), "Oui");}

    @Test
    public void setAnswer() {quest1.setAnswerA("Réponse AA"); assertNotEquals(quest1.getAnswerA(), "Réponse A"); quest1.setAnswerB("Réponse BB"); assertNotEquals(quest1.getAnswerB(), "Réponse B"); quest1.setAnswerC("Réponse CC"); assertNotEquals(quest1.getAnswerC(), "Réponse C"); quest1.setAnswerD("Réponse DD"); assertNotEquals(quest1.getAnswerD(), "Réponse D");}
}
