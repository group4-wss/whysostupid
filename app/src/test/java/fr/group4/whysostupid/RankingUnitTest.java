package fr.group4.whysostupid;

import org.hamcrest.core.IsEqual;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RankingUnitTest {
    Ranking rank1;

    @Before
    public void setUpNewRank(){
        rank1 = new Ranking("Test", 4);
    }

    @Test
    public void getScore() {assertEquals(rank1.getScore(), 4);}

    @Test
    public void getUsername() {assertEquals(rank1.getUsername(), "Test");}

    @Test
    public void setScore() {rank1.setScore(2); assertNotEquals(rank1.getScore(), 4);}

    @Test
    public void setUsername() {rank1.setUsername("Test2"); assertNotEquals(rank1.getUsername(), "Test");}
}
