package fr.group4.whysostupid;

import org.hamcrest.core.IsEqual;
import org.junit.Before;
import org.junit.Test;

import fr.group4.whysostupid.Model.Category;
import fr.group4.whysostupid.Model.User;

import static org.junit.Assert.*;

public class UserUnitTest {
    User user1;

    @Before
    public void setUpUser() {user1 = new User("Name", "pwd", "email");}

    @Test
    public void getUsername() {assertEquals(user1.getUsername(), "Name");}

    @Test
    public void getPassword() {assertEquals(user1.getPassword(), "pwd");}

    @Test
    public void getEmail() {assertEquals(user1.getEmail(), "email");}

    @Test
    public void setUsername() {user1.setUsername("Name2"); assertNotEquals(user1.getUsername(), "Name");}

    @Test
    public void setPassword() {user1.setPassword("pwd2"); assertNotEquals(user1.getPassword(), "pwd");}

    @Test
    public void setEmail() {user1.setEmail("email2"); assertNotEquals(user1.getEmail(), "email");}
}
