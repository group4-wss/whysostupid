package fr.group4.whysostupid;

import org.hamcrest.core.IsEqual;
import org.junit.Before;
import org.junit.Test;

import fr.group4.whysostupid.Model.QuestionScore;

import static org.junit.Assert.*;

public class QuestionScoreUnitTest {
    QuestionScore questScore1;

    @Before
    public void setUpQuestionScore() {questScore1 = new QuestionScore("100", "User", "10", "1", "Nom Categorie");}

    @Test
    public void getQuestionScore() {assertEquals(questScore1.getQuestion_Score(), "100"); assertEquals(questScore1.getUser(), "User"); assertEquals(questScore1.getScore(), "10"); assertEquals(questScore1.getCategoryId(), "1"); assertEquals(questScore1.getCategoryName(), "Nom Categorie");}

    @Test
    public void setQuestionScore() {questScore1.setQuestion_Score("200"); assertNotEquals(questScore1.getQuestion_Score(), "100"); questScore1.setUser("User2"); assertNotEquals(questScore1.getUser(), "User"); questScore1.setScore("20"); assertNotEquals(questScore1.getScore(), "10"); questScore1.setCategoryId("2"); assertNotEquals(questScore1.getCategoryId(), "1"); questScore1.setCategoryName("Nom Categorie 2"); assertNotEquals(questScore1.getCategoryName(), "Nom Categorie");}
}
