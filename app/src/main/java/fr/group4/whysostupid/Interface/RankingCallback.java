package fr.group4.whysostupid.Interface;

import fr.group4.whysostupid.Ranking;

public interface RankingCallback<T> {
    void callBack(T ranking);

}
