package fr.group4.whysostupid.Stripe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import fr.group4.whysostupid.Common.Common;
import fr.group4.whysostupid.Done;
import fr.group4.whysostupid.Home;
import fr.group4.whysostupid.R;

public class Activity_payment_summary extends AppCompatActivity {

    // Variable declaration
    Button buttonConfirm;
    TextView txtPrice;
    FirebaseDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_summary);

        txtPrice = (TextView)findViewById(R.id.txtPrice);

        buttonConfirm = (Button)findViewById(R.id.buttonConfirm);

        // Firebase
        database = FirebaseDatabase.getInstance();

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Activity_payment_summary.this, Home.class);
                startActivity(intent);
                finish();
            }
        });

        // Get data from bundle and set to view
        Bundle extra = getIntent().getExtras();
        if (extra != null){
            String idCategory = extra.getString("IdCategory");
            String currency = extra.getString("CURRENCY");
            String amount = extra.getString("AMOUNT");
            float amountFloat = Float.parseFloat(amount);

            System.out.println("Current user " + Common.currentUser.getUsername());
            database.getReference("Category").child(idCategory).child("Subscribe")
                    .child(Common.currentUser.getUsername())
                    .setValue(Common.currentUser.getUsername());

            txtPrice.setText(String.format("Price : %s %s", amountFloat / 100, currency));
        }
    }
}