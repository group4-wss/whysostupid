package fr.group4.whysostupid.ViewHolder;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import fr.group4.whysostupid.R;
import fr.group4.whysostupid.Stripe.CheckoutActivityJava;

// Load activity_checkout view to do Stripe transaction
public class StripeFragment extends Fragment {

    // Variable declaration
    String category;

    // Instance called from menu navigation bar
    public static StripeFragment newInstance(){
        StripeFragment stripeFragment = new StripeFragment();
        return stripeFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_stripe_launcher, container, false);

        // Instantiate category shop button
        Button stripe_category_1 = (Button) view.findViewById(R.id.stripe_category_1);
        Button stripe_category_2 = (Button) view.findViewById(R.id.stripe_category_2);

        // Category 1 purchase button event listener
        stripe_category_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Display in Android Studio Run window
                // When we are going to buy a category
                System.out.println("Watch category 1 sale");
                // Start payment view called by CheckoutActivityJava
                Intent startStripe = new Intent(getActivity(), CheckoutActivityJava.class);

                // Set data value will be send to checkout activity java
                Bundle dataSend = new Bundle();
                category = "Alternadom";
                dataSend.putString("CATEGORY", category);
                startStripe.putExtras(dataSend);

                startActivity(startStripe);
            }
        });

        // Category 1 purchase button event listener
        stripe_category_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Display in Android Studio Run window
                // When we are going to buy a category
                System.out.println("Watch category 2 sale");
                // Start payment view called by CheckoutActivityJava
                Intent startStripe = new Intent(getActivity(), CheckoutActivityJava.class);

                // Set data value will be send to checkout activity java
                Bundle dataSend = new Bundle();
                category = "Animaux";
                dataSend.putString("CATEGORY", category);
                startStripe.putExtras(dataSend);

                startActivity(startStripe);
            }
        });

        return view;
    }



}