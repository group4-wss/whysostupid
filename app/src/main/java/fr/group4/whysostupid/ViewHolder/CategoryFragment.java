package fr.group4.whysostupid.ViewHolder;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;

import fr.group4.whysostupid.Common.Common;
import fr.group4.whysostupid.Interface.ItemClickListener;
import fr.group4.whysostupid.R;
import fr.group4.whysostupid.Start;
import fr.group4.whysostupid.Model.Category;

/*
* We create fragment for represent a reusable portion of own app
* */
public class CategoryFragment extends Fragment {

    View myFragment;

    RecyclerView listCategory;
    RecyclerView.LayoutManager layoutManager;
    FirebaseRecyclerAdapter<Category, CategoryViewHolder> adapter;

    FirebaseDatabase database;
    //DatabaseReference categories;
    Query categories;

    public static CategoryFragment newInstance(){
        CategoryFragment categoryFragment = new CategoryFragment();
        return categoryFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        database = FirebaseDatabase.getInstance();
        categories = database.getReference("Category").orderByKey();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        myFragment = inflater.inflate(R.layout.fragment_category, container, false);

        // Find categories on firebase
        listCategory = (RecyclerView)myFragment.findViewById(R.id.listCategory);
        listCategory.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(container.getContext());
        listCategory.setLayoutManager(layoutManager);
        
        loadCategories();
        
        return myFragment;
    }

    private void loadCategories() {
        adapter = new FirebaseRecyclerAdapter<Category, CategoryViewHolder>(
                Category.class,
                R.layout.category_layout,
                CategoryViewHolder.class,
                categories
        ) {
            @Override
            protected void populateViewHolder(CategoryViewHolder categoryViewHolder, final Category category, int i) {
                System.out.println("Current user " + Common.currentUser.getUsername());
                System.out.println("Category subscribe list " + category.getSubscribe());
                System.out.println("Category full object " + category);

                // Compare BDD list with the current user before showing categories
                if(category.getSubscribe().toString().contains(Common.currentUser.getUsername())) {

                    // Get name of category
                    categoryViewHolder.category_name.setText(category.getName());
                    // With Picasso, we load image from internet
                    Picasso.with(getActivity())
                            .load(category.getImage())
                            .into(categoryViewHolder.category_image);

                    categoryViewHolder.setItemClickListener(new ItemClickListener() {
                        @Override
                        public void onClick(View view, int position, boolean isLongClick) {
                            // When we click on category image, we display name and position
                            //Toast.makeText(getActivity(), String.format("%s|%s", adapter.getRef(position).getKey(), category.getName()), Toast.LENGTH_SHORT).show();
                            // When we click on categories, we redirect on category game quiz
                            Intent startGame = new Intent(getActivity(), Start.class);
                            Common.categoryId = adapter.getRef(position).getKey();
                            Common.categoryName = category.getName();
                            startActivity(startGame);

                        }
                    });
                } else {
                    // Get name of category
                    categoryViewHolder.category_name.setText(category.getName());

                    // Load premium picture
                    Picasso.with(getActivity())
                            .load("https://image.freepik.com/vecteurs-libre/collection-premium-insigne-or-conception-etiquette_1017-7701.jpg")
                            .into(categoryViewHolder.category_image);

                    categoryViewHolder.setItemClickListener(new ItemClickListener() {
                        @Override
                        public void onClick(View view, int position, boolean isLongClick) {
                            // Do nothing
                        }
                    });
                }

            }
        };
        adapter.notifyDataSetChanged();
        listCategory.setAdapter(adapter);
    }
}
