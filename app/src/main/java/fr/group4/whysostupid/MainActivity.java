package fr.group4.whysostupid;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import fr.group4.whysostupid.AESUtilsClass.AESUtils;
import fr.group4.whysostupid.Common.Common;
import fr.group4.whysostupid.Model.User;

public class MainActivity extends AppCompatActivity {
    MaterialEditText edtNewUsername, edtNewPassword, edtNewEmail; // Declare Register Fields
    MaterialEditText edtUsername, edtPassword; // Declare Login Fields
    String edtPasswordEncrypted;
    String edtPasswordDecrypted;
    String test;
    Button btn_login, btn_register;


    // Set Database
    FirebaseDatabase database;
    DatabaseReference users;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Firebase
        database = FirebaseDatabase.getInstance();
        users = database.getReference("User");

        edtUsername  = findViewById(R.id.edtUsername);
        edtPassword  = findViewById(R.id.edtPassword);

        btn_login    = findViewById(R.id.btn_login);
        btn_register = findViewById(R.id.btn_register);

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRegisterDialog();
            }
        });

        btn_login.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login(edtUsername.getText().toString(),
                        edtPassword.getText().toString());
            }
        }));
    }


    // Launch login process
    private void login(final String username, final String password) {
        users.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                // Ensure that user exists to connect
                if (snapshot.child(username).exists()) {
                    if (!username.isEmpty()) {
                        User login = snapshot.child(username).getValue(User.class);
                        try {
                            edtPasswordDecrypted = AESUtils.decrypt(login.getPassword());

                            System.out.println("If Login.getpassword " + edtPasswordDecrypted  + " " + password);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //System.out.println("If Login.getpassword " + login.getPassword() + " " + password);
                        if (edtPasswordDecrypted.equals(password)) {
                            // Return Informational message on good login
                            Toast.makeText(MainActivity.this,
                                    "Login successful",
                                    Toast.LENGTH_SHORT).show();
                            // Redirect on home screen
                            Intent homeActivity = new Intent(MainActivity.this, Home.class);
                            Common.currentUser = login; // We set current user
                            startActivity(homeActivity);
                            finish();
                        } else {
                            // Return Error message on bad login
                            Toast.makeText(MainActivity.this,
                                    "Failed to login : Wrong password",
                                    Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        // Return Error message on empty username field
                        Toast.makeText(MainActivity.this,
                                "Failed to login : Username field is empty",
                                Toast.LENGTH_SHORT).show();
                    }
                } else {
                    // Return Error message if user don't exists
                    Toast.makeText(MainActivity.this,
                            "Failed to login : User not exists",
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    // Launch register process
    private void showRegisterDialog() {

        AlertDialog.Builder alterDialog = new AlertDialog.Builder(MainActivity.this, R.style.AlertDialogStyle);
        alterDialog.setTitle("Register");
        alterDialog.setMessage("Please fill register form");

        LayoutInflater inflater = this.getLayoutInflater();
        View register_layout = inflater.inflate(R.layout.register_layout, null);

        // Map Fields
        edtNewUsername = register_layout.findViewById(R.id.edtNewUsername);
        edtNewPassword = register_layout.findViewById(R.id.edtNewPassword);
        edtNewEmail    = register_layout.findViewById(R.id.edtNewEmail);

        alterDialog.setView(register_layout); // Set the view
        alterDialog.setIcon(R.drawable.ic_account_circle_black_24dp); // Set account icon

        alterDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alterDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Get Register form values to String
                try {

                    edtPasswordEncrypted = AESUtils.encrypt(edtNewPassword.getText().toString());
                    System.out.println("If register  " + edtPasswordEncrypted);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                final User user = new User(
                    edtNewUsername.getText().toString(),
                    edtPasswordEncrypted,
                    edtNewEmail.getText().toString()
                );
                users.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        // Ensure that user doesn't exists before create
                        if (snapshot.child(user.getUsername()).exists()) {
                            // Return Error message
                            Toast.makeText(MainActivity.this,
                                    "User Already Exists",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            // Add free category when a registration user
                            String categoryIndex;
                            for (int i =1; i < 7; i++){

                                if(i < 9) { categoryIndex = "0" + String.valueOf(i); }
                                else { categoryIndex = String.valueOf(i);}

                                    database.getReference("Category").child(categoryIndex).child("Subscribe")
                                        .child(edtNewUsername.getText().toString())
                                        .setValue(edtNewUsername.getText().toString());
                            }
                            // Associate user values
                            users.child(user.getUsername()).setValue(user);
                            // Return Informational message
                            Toast.makeText(MainActivity.this,
                                    "User Registration successful",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
                dialog.dismiss();
            }
        });

        alterDialog.show();

    }
}
