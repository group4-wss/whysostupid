package fr.group4.whysostupid;

public class Ranking {
    private String username;
    private long score;

    public Ranking() {
    }

    public Ranking(String username, long score) {
        this.username = username;
        this.score = score;
    }

    // Ranking Username Getters and

    public String getUsername() { return username; }
    public void setUsername(String username) { this.username = username; }

    // Ranking Score Getters and Setters

    public long getScore() { return score; }
    public void setScore(long score) { this.score = score; }




}
