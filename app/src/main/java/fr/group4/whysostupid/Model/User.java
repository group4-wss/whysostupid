package fr.group4.whysostupid.Model;

public class User {
    private String username;
    private String password;
    private String email;

    public User() {

    }

    public User(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email    = email;
    }

    // Username Getters and Setters

    public String getUsername() { return username; }
    public void setUsername(String username) { this.username = username; }

    // Password Getters and Setters

    public String getPassword() { return password; }
    public void setPassword(String password) { this.password = password; }

    // Email Getters and Setters

    public String getEmail() { return email; }
    public void setEmail(String email) { this.email = email; }
}
