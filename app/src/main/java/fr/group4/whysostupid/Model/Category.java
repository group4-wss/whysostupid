package fr.group4.whysostupid.Model;

import fr.group4.whysostupid.Common.Common;

public class Category {
    private Object Subscribe;
    private String Name;
    private String Image;

    public Category(){

    }

    public Category(String name, String image, Object subscribe){
        Name = name;
        Image = image;
        Subscribe = subscribe;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public Object getSubscribe() { return Subscribe; }

    public void setSubscribe(Object subscribe) { Subscribe = subscribe; }
}
