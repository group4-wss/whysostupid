package fr.group4.whysostupid.Common;

import java.util.ArrayList;
import java.util.List;

import fr.group4.whysostupid.Model.Question;
import fr.group4.whysostupid.Model.User;

/*
* We create fr.group4.whysostupid.Common.java class to store some global variable
* */
public class Common {
    public static String categoryId, categoryName;
    public static User currentUser;
    public static List<Question> questionList = new ArrayList<>(); // We create global variable list of question
}
